<?php

namespace Belvg\Test\Model\Data;

use Belvg\Test\Api\Data\BlogInterface;
use Magento\Framework\Api\AbstractExtensibleObject;

/**
 * Class Blog
 * @package Belvg\Test\Model\Data
 */
class Blog extends AbstractExtensibleObject implements BlogInterface
{
    /**
     * Get blog_id
     * @return string|null
     */
    public function getBlogId()
    {
        return $this->_get(self::BLOG_ID);
    }

    /**
     * Set blog_id
     * @param string $blogId
     * @return BlogInterface
     */
    public function setBlogId($blogId)
    {
        return $this->setData(self::BLOG_ID, $blogId);
    }

    /**
     * Get title
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     * @param string $title
     * @return BlogInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get description
     * @return string|null
     */
    public function getDescription()
    {
        return $this->_get(self::DESCRIPTION);
    }

    /**
     * @param string $description
     * @return BlogInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Belvg\Test\Api\Data\BlogExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Belvg\Test\Api\Data\BlogExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Belvg\Test\Api\Data\BlogExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
