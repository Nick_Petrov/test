<?php

namespace Belvg\Test\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;

/**
 * Class Blog
 * @package Belvg\Test\Controller\Adminhtml
 */
abstract class Blog extends Action
{
    const ADMIN_RESOURCE = 'Belvg_Test::top_level';

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Blog constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Belvg'), __('Belvg'))
            ->addBreadcrumb(__('Blog'), __('Blog'));
        return $resultPage;
    }
}
