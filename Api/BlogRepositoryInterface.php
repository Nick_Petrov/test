<?php

namespace Belvg\Test\Api;

use Belvg\Test\Api\Data\BlogInterface;
use Belvg\Test\Api\Data\BlogSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Interface BlogRepositoryInterface
 * @package Belvg\Test\Api
 */
interface BlogRepositoryInterface
{

    /**
     * Save Blog
     * @param BlogInterface $blog
     * @return BlogInterface
     * @throws LocalizedException
     */
    public function save(BlogInterface $blog);

    /**
     * Retrieve Blog
     * @param string $blogId
     * @return BlogInterface
     * @throws LocalizedException
     */
    public function getById($blogId);

    /**
     * Retrieve Blog matching the specified criteria.
     * @param SearchCriteriaInterface $searchCriteria
     * @return BlogSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Blog
     * @param BlogInterface $blog
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(BlogInterface $blog);

    /**
     * Delete Blog by ID
     * @param string $blogId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($blogId);
}
