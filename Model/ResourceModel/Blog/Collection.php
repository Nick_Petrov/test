<?php

namespace Belvg\Test\Model\ResourceModel\Blog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Belvg\Test\Model\Blog;
use Belvg\Test\Model\ResourceModel\Blog as ResourceBlog;

/**
 * Class Collection
 * @package Belvg\Test\Model\ResourceModel\Blog
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Blog::class,
            ResourceBlog::class
        );
    }
}
