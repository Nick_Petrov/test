<?php

namespace Belvg\Test\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface BlogSearchResultsInterface
 * @package Belvg\Test\Api\Data
 */
interface BlogSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Blog list.
     * @return \Belvg\Test\Api\Data\BlogInterface[]
     */
    public function getItems();

    /**
     * Set title list.
     * @param \Belvg\Test\Api\Data\BlogInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
