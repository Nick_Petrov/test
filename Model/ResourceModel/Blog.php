<?php

namespace Belvg\Test\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Blog
 * @package Belvg\Test\Model\ResourceModel
 */
class Blog extends AbstractDb
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('belvg_test_blog', 'blog_id');
    }
}
