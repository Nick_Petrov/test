<?php

namespace Belvg\Test\Model;

use Belvg\Test\Api\Data\BlogInterface;
use Belvg\Test\Api\Data\BlogInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Belvg\Test\Model\ResourceModel\Blog as ResourceBlog;
use Belvg\Test\Model\ResourceModel\Blog\Collection;

class Blog extends AbstractModel
{
    /**
     * @var BlogInterfaceFactory
     */
    protected $blogDataFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    protected $_eventPrefix = 'belvg_test_blog';

    /**
     * Blog constructor.
     * @param Context $context
     * @param Registry $registry
     * @param BlogInterfaceFactory $blogDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ResourceBlog $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        BlogInterfaceFactory $blogDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceBlog $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->blogDataFactory = $blogDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve blog model with blog data
     * @return BlogInterface
     */
    public function getDataModel()
    {
        $blogData = $this->getData();
        
        $blogDataObject = $this->blogDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $blogDataObject,
            $blogData,
            BlogInterface::class
        );
        
        return $blogDataObject;
    }
}
