<?php

namespace Belvg\Test\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface BlogInterface
 * @package Belvg\Test\Api\Data
 */
interface BlogInterface extends ExtensibleDataInterface
{

    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const BLOG_ID = 'blog_id';

    /**
     * Get blog_id
     * @return string|null
     */
    public function getBlogId();

    /**
     * Set blog_id
     * @param string $blogId
     * @return BlogInterface
     */
    public function setBlogId($blogId);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return BlogInterface
     */
    public function setTitle($title);

    /**
     * Get description
     * @return string|null
     */
    public function getDescription();

    /**
     * Set description
     * @param string $description
     * @return BlogInterface
     */
    public function setDescription($description);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Belvg\Test\Api\Data\BlogExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Belvg\Test\Api\Data\BlogExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Belvg\Test\Api\Data\BlogExtensionInterface $extensionAttributes
    );
}
